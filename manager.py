from telegram.ext import CommandHandler, MessageHandler, Updater, ConversationHandler, Filters, CallbackQueryHandler

from handlers import start, choice, query_handler, get_user_name, create_user
from config import TOKEN, request_kwargs
from openvpn import init

import logging


logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
            )

logger = logging.getLogger(__name__)

def main():
    updater = Updater(TOKEN, request_kwargs=request_kwargs)
    dp = updater.dispatcher

    dp.add_handler(ConversationHandler(
        entry_points = [CommandHandler('start', start)],
        states = {
            1: [MessageHandler(Filters.regex('^(Manage current clients|Add a new Client)$'), choice), MessageHandler(Filters.all, start)],
            2: [MessageHandler(Filters.text, get_user_name)],
            3: [MessageHandler(Filters.regex('^(\/skip|[0-9]+)$'), create_user)]
        },
        fallbacks = []
    ))

    dp.add_handler(CallbackQueryHandler(query_handler))

    init()

    updater.start_polling()
    updater.idle()

if __name__=='__main__':
    main()
