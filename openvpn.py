import os
import sqlite3
import datetime
import pytz
import sys
import re


if not os.path.isdir('configs'):
    os.mkdir('configs')

users = [str(i) for i in range(42)]

with open('/etc/openvpn/client-template.txt', 'r') as f:
    CONFIG_FORMAT = f"{f.read()}\n" + '\n'.join([f'<{i}>\n{{{i}}}\n</{i}>' for i in ['ca', 'cert', 'key']])
home_dir = os.path.abspath('.')

def init():
    global CONFIG_FORMAT
    init_db()

    with open('/etc/openvpn/server.conf') as server_conf:
        data = server_conf.read()
        if any([i.startswith('tls-crypt') for i in data.split('\n')]):
            with open('/etc/openvpn/tls-crypt.key', 'r') as f:
                CONFIG_FORMAT += f'\n<tls-crypt>\n{f.read()}\n</tls-crypt>'

        elif any([i.startswith('tls-auth') for i in data.split('\n')]):
            with open('/etc/openvpn/tls-auth.key', 'r') as f:
                CONFIG_FORMAT += f'\n<tls-auth>\n{f.read()}\n</tls-auth>'
        
        else:
            server_conf.close()
            sys.exit(1)
    
    with open("/etc/openvpn/easy-rsa/pki/ca.crt", 'r') as ca:
        CONFIG_FORMAT = CONFIG_FORMAT.replace('{ca}', ca.read())

    with open('/etc/openvpn/easy-rsa/pki/index.txt', 'r') as f:
        content = '\n'.join([i for i in f.readlines() if i.startswith('V')])

    old_users = re.findall(r'/CN=([A-Z a-z \- 0-9]+)', content)
    for user in old_users:
        if user == 'server':
            continue
        if user in get_users():
            continue

        add_user(user)

def init_db():
    exec_db('''CREATE TABLE IF NOT EXISTS users(
            name text PRIMARY KEY NOT NULL,
            register_date text NOT NULL,
            days INT, telegram_id INT)''', commit=True)

def exec_db(cmd, args=None, fetch=False, commit=False):
    with sqlite3.connect(f'{home_dir}/db.sqlite3') as db:
        c = db.cursor()
        if args:
            data = c.execute(cmd, args)
        else:
            data = c.execute(cmd)
        if commit:
            db.commit()        
        if fetch:
            data = list(data)
        c.close()

    if fetch:
        return list(data)

    return True

def get_users() -> list:
    users = exec_db('SELECT name FROM users', fetch=True)
    return [str(user[0]) for user in users]

def add_user(name: str, days=None, db_only=False) -> bool:
    if name in get_users():
        return False
    exec_db('INSERT INTO users(name, register_date, days) VALUES(?, ?, ?)', (name, datetime.datetime.now().astimezone(tz=pytz.timezone('Asia/Tehran')).strftime('%Y/%m/%d %H:%M'), days), commit=True)

    if not db_only:
        os.chdir('/etc/openvpn/easy-rsa/')
        os.system(f'./easyrsa build-client-full "{name}" nopass')
        os.chdir(home_dir)

    create_config(name)
    users.append(name)
    return True

def revoke(name) -> str:
    if name in get_users():
        exec_db("DELETE FROM users WHERE name=?", (name,), commit=True)
        os.chdir('/etc/openvpn/easy-rsa/')
        os.system(f'./easyrsa --batch revoke "{name}"')
        os.system('EASYRSA_CRL_DAYS=3650 ./easyrsa gen-crl')
        os.system('rm -f /etc/openvpn/crl.pem')
        os.system('cp /etc/openvpn/easy-rsa/pki/crl.pem /etc/openvpn/crl.pem')
        os.system('chmod 644 /etc/openvpn/crl.pem')
        fn = f'{home_dir}/configs/{name}.ovpn'
        if os.path.isfile(fn):
            os.remove(fn)
        os.system(f'sed -i "/^{name},.*/d" /etc/openvpn/ipp.txt')
        return True
    return False

def create_config(name):
    with open(f'/etc/openvpn/easy-rsa/pki/issued/{name}.crt', 'r') as cert_file:
        cert = cert_file.read()
        cert = cert[cert.find('-----BEGIN CERTIFICATE-----'):]
        
    with open(f'/etc/openvpn/easy-rsa/pki/private/{name}.key', 'r') as key_file:
        key = key_file.read()
    
    with open(f'{home_dir}/configs/{name}.ovpn', 'w') as config_file:
        config_file.write(CONFIG_FORMAT.format(cert=cert, key=key))

def get_config(name):
    if name in get_users():
        if not os.path.exists(f'configs/{name}.ovpn'):
            create_config(name)
        return f'{name}.ovpn'
    return False
